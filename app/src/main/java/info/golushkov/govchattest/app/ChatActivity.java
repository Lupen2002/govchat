package info.golushkov.govchattest.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import info.golushkov.govchattest.app.core.Message;
import info.golushkov.govchattest.app.core.Messager;
import info.golushkov.govchattest.app.core.SendTask;
import info.golushkov.govchattest.app.core.impl.TextMessage;

public class ChatActivity extends Activity implements View.OnClickListener {
    private ListView listMessage;
    private Messager messager = Messager.getInstance();
    private EditText messText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);
        listMessage = (ListView) findViewById(R.id.list_messages);
        listMessage.setAdapter(messager.getAdapter());
        Button button = (Button) findViewById(R.id.send_btn);
        button.setOnClickListener(this);
        messText = (EditText) findViewById(R.id.new_mess_text);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.send_btn:
                String text = messText.getText().toString();
                if (text.length() > 1){
                    TextMessage message = new TextMessage();
                    message.setOwner(messager.getLogin());
                    message.setStatus(Message.Status.MyNew);
                    message.setType(Message.TYPE_MESSAGE);
                    message.setText(text);
                    new SendTask().execute(message);
                    messText.getText().clear();
                }
                break;
        }
    }

}
