package info.golushkov.govchattest.app.core;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public interface Message {
    enum Status {
        MyNew, MySend, MyRecd, MyRead,
        New, Read
    }
    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_INFO = 1;
    public static final int TYPE_ERROR = 2;
    String getOwner();
    String getText();
    Date getDate();
    Status getStatus();
    int getType();
    void setStatus(Status status);
    JSONObject toJSON() throws JSONException;
}
