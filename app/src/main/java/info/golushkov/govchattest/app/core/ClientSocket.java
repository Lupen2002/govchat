package info.golushkov.govchattest.app.core;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.engineio.client.EngineIOException;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class ClientSocket {
    private static ClientSocket instance = null;
    private Socket socket;
    private static final String BASE_URL = "http://128.199.34.217:8124/";

    public static synchronized ClientSocket getInstance() {
        if (instance == null)
            instance = new ClientSocket();
        return instance;
    }

    private ClientSocket(){
        try {
            IO.Options o = new IO.Options();
            o.port = 8124;
            socket = IO.socket(BASE_URL);
            socket.on(Socket.EVENT_MESSAGE,new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    ArrayList<Message> messages = new ArrayList<Message>(args.length);
                    for (int i = 0; i < args.length; i++) {
                        if (args[i] instanceof String) {
                            String jsonStr = (String) args[i];
                            Message message = MessageFactory.make(jsonStr);
                            messages.add(message);
                        } else if (args[i] instanceof JSONObject) {
                            JSONObject json = (JSONObject) args[i];
                            Message message = MessageFactory.make(json);
                            messages.add(message);
                        }
                    }
                    new PrintMessTask().execute(messages);
                }
            });
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(Message message) {
        try {
            socket.send(message.toJSON());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
