package info.golushkov.govchattest.app.core;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

public class SendTask extends AsyncTask<Message, Void, Message[]> {

    @Override
    protected Message[] doInBackground(Message... params) {
        ClientSocket socket = ClientSocket.getInstance();
        for (int i = 0; i < params.length; i++) {
            socket.sendMessage(params[i]);
        }
        return params;
    }

    @Override
    protected void onPostExecute(Message[] messages) {
        super.onPostExecute(messages);
        Messager messager = Messager.getInstance();
        for (int i = 0; i < messages.length; i++) {
            messager.addMessage(messages[i]);
        }
    }

}
