package info.golushkov.govchattest.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import info.golushkov.govchattest.app.core.ClientSocket;
import info.golushkov.govchattest.app.core.Message;
import info.golushkov.govchattest.app.core.Messager;
import info.golushkov.govchattest.app.core.SendTask;
import info.golushkov.govchattest.app.core.impl.TextMessage;


public class MainActivity extends Activity implements View.OnClickListener {
    private EditText login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Messager.getInstance().init(this);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.login_btn);
        button.setOnClickListener(this);
        login = (EditText) findViewById(R.id.login_in_mess);
        ClientSocket.getInstance();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                String loginName = login.getText().toString();
                if (loginName.length() > 1){
                    TextMessage message = new TextMessage();
                    Messager.getInstance().setLogin(loginName);
                    message.setOwner(loginName);
                    message.setStatus(Message.Status.MyNew);
                    message.setType(Message.TYPE_INFO);
                    message.setText("Новый пользователь: " + loginName);
                    new SendTask().execute(message);
                    Intent intent = new Intent(this, ChatActivity.class);
                    startActivity(intent);
                }
                break;
        }
    }
}
