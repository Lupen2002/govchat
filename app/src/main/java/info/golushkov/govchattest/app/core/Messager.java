package info.golushkov.govchattest.app.core;

import android.content.Context;
import android.widget.SimpleAdapter;
import info.golushkov.govchattest.app.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Messager {
    public static final String ATTR_LOGIN = "login";
    public static final String ATTR_DATE = "date";
    public static final String ATTR_TEXT = "text";

    private ArrayList<Map<String,String>> listMessage = new ArrayList<Map<String, String>>();
    private SimpleAdapter adapter;
    private static Messager instance = null;
    private String login;

    public static synchronized Messager getInstance() {
        if (instance == null)
            instance = new Messager();
        return instance;
    }

    public void addMessage(Message message){
        Map m = new HashMap(3);
        m.put(ATTR_DATE,String.valueOf(message.getDate().getTime()));
        m.put(ATTR_LOGIN,message.getOwner());
        m.put(ATTR_TEXT,message.getText());
        listMessage.add(m);
        adapter.notifyDataSetChanged();
    }

    public void init(Context context){
        String from[] = {ATTR_DATE, ATTR_LOGIN, ATTR_TEXT};
        int to[] = {R.id.date_in_mess, R.id.login_in_mess, R.id.text_in_mess};
        adapter = new SimpleAdapter(context,listMessage,R.layout.message_item,from,to);
    }

    public SimpleAdapter getAdapter() {
        return adapter;
    }

    private Messager(){

    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
