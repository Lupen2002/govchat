package info.golushkov.govchattest.app.core;

import android.os.AsyncTask;

import java.util.List;

public class PrintMessTask extends AsyncTask<List<Message>, Message, Void> {
    @Override
    protected Void doInBackground(List<Message>... params) {
        for (int i = 0; i < params.length; i++) {
            List<Message> messages = params[i];
            for (int j = 0; j < messages.size(); j++) {
                Message message = messages.get(j);
                publishProgress(message);
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Message... values) {
        super.onProgressUpdate(values);
        Messager messager = Messager.getInstance();
        for (int i = 0; i < values.length; i++) {
            messager.addMessage(values[i]);
        }
    }
}
