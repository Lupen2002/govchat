package info.golushkov.govchattest.app.core.impl;

import info.golushkov.govchattest.app.core.Message;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class TextMessage implements Message{
    private String owner;
    private String text;
    private Date date;
    private Status status;
    private int type;

    public TextMessage(){
        date = new Date();
    }

    @Override
    public String getOwner() {
        return owner;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("owner",getOwner());
        json.put("data",getDate().getTime());
        json.put("text",getText());
        json.put("type",getType());
        return json;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setDate(long date) {
        this.date = new Date(date);
    }
}
