package info.golushkov.govchattest.app.core;

import info.golushkov.govchattest.app.core.impl.TextMessage;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageFactory {
    public static Message make(String jsonStr) {
        Message message = null;
        try {
            JSONObject json = new JSONObject(jsonStr);
            message = make(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }

    public static Message make(JSONObject json) {
        TextMessage message = null;
        try {
            message = new TextMessage();
            message.setDate(json.getLong("data"));
            message.setOwner(json.getString("owner"));
            message.setText(json.getString("text"));
            message.setType(json.getInt("type"));
            message.setStatus(Message.Status.New);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }
}

